// alert("Hello World! B249");

// Create Array
let name = ['timmy', 'jane', 'joe'];
console.log(name);

// Add to Array
name.push('jimmy');
console.log(name);

//unshift - add to beginning of array
name.unshift('DOnna');
console.log(name);

//pop - remove from end of array
name.pop();
console.log(name);

//shift - remove from beginning of array
name.shift();
console.log(name);

//splice - remove from middle of array
name.splice(1, 1);
console.log('SPLICE',name);

//slice - copy array
name = ['timmy', 'jane', 'joe'];
let name2 = name.slice(0, 2);
console.log('SLICE',name2);

//sort - sort array
name.sort();
console.log(name);

//reverse - reverse array
name2.reverse();
console.log(name2);

//indexOf - find index of item in array
console.log(name.indexOf('jane'));

//forEach - loop through array
name.forEach(function(item, index){
    console.log(item, index);
});

//map - create new array from existing array
let name3 = name.map(function(item){
    return item;
});
console.log(name3);

// every - check if all items in array meet condition
let name4 = name.every(function(item){
    return item.length > 4;
}   
);
console.log(name4);

// some - check if some items in array meet condition
let name5 = name.some(function(item){
    return item.length > 4;
}   
);
console.log(name5);

let arrNum = [15, 20, 25, 30 ,11,7];
// check if divisible by five
arrNum.forEach(function(item){
    // display if item is devisibele by 5
    if (item % 5 === 0) {
        console.log(item + ' is divisible by 5');
    } else {
        console.log(item + ' is not divisible by 5');
    }
});

// filter - create new array from existing array based on condition
let name6 = name.filter(function(item){
    return item.length > 4;
}
);

console.log(name6);

// reduce - reduce array to single value
let name7 = name.reduce(function(prev, curr){
    return prev + curr;
}
);

console.log(name7);

// Match Object - allows to use regular expressions

let str = 'Hello World';
let result = str.match(/hello/i);
console.log(result);

console.log(Math.PI); //PI = 3.14
console.log(Math.E); //E = 2.71 i.e. natural log.
console.log(Math.round(2.4)); //round
console.log(Math.ceil(2.4)); //round up
console.log(Math.floor(2.4)); //round down
console.log(Math.sqrt(64)); //square root of 64 = 8
console.log(Math.abs(-3)); //absolute value
console.log(Math.pow(8, 2)); //power
console.log(Math.min(2, 33, 4, 1, 55, 6, 3, -2)); //min value of array of numbers is -2
console.log(Math.max(2, 33, 4, 1, 55, 6, 3, -2)); //max value
console.log(Math.random()); //random number between 0 and 1
// truncate decimal part of a number (remove decimal part)
console.log('TRUNC',Math.trunc(13.37));
// natural logarithm of 2
console.log(Math.log(2));
// 10 to the power of 2
console.log(Math.pow(10, 2));
// random number between 0 and 1
console.log(Math.random());

console.log(Math.round(Math.PI));
console.log(Math.ceil(Math.PI));
console.log(Math.min(-1,-2,-4,-100,0,1,2));


// Activity QUIZ
// #1 How do you create arrays in Javascript? 
//  ANSWER: let name = ['timmy', 'jane', 'joe'];

// #2 How do you access the first character of an array?
//  ANSWER: name[0]; //index starts at 0

// #3 How do you access the last character of an array?
//  ANSWER: name[name.length - 1]; //length starts at 1

// #4 What array method searches for and return the index of a given value in an array?
// Answer: indexOf

// #5 What array method loops over all elements of an array, performing a user-defined function on each iteration?
// Answer: forEach

// #6 What array method creates a new array with elements obtained from a user-defined function?
// Answer: map

// #7 What array method checks if all its elements satisfy a given condition?
// Answer: every

// #8 What array method checks if at least one of its elements satisfies a given condition?
// Answer: some

// #9 True or False: array.splice() modifies a copy of the array, leaving the original unchanged?
// Answer: False

//  #10 True or False: array.slice() copies elements from original array and returns these as a new array.
// Answer: True



//==================Activity - Function Coding
//#1
// Create a function named addToEnd that will add a passed in string to the end of a passed in array.

function addToEnd(students, str) {
    // If element to be added is not a string,return the string "error - can only add strings to an array".
    if (typeof str !== 'string') {
        return 'error - can only add strings to an array';
    } else {
        // array". Otherwise,return the updated array.
        students.push(str);
        return students;
    }
}

// Use AddToend
let students = ['John', 'Joe','Jane', 'Jessie'];
console.log(addToEnd(students, "Ryan"));

// Test for Error
console.log(addToEnd(students, 045));


//#2
// Create a function named addToStart that will add a passed in string to the start of a passed in array.
function addToStart(students, str) {
    // If element to be added is not a string,return the string "error - can only add strings to an array".
    if (typeof str !== 'string') {
        return 'error - can only add strings to an array';
    } else {
        // array". Otherwise,return the updated array.
        students.unshift(str);
        return students;
    }
}

// Use AddToStart
console.log(addToStart(students, "Tess"));

// Test for Error
console.log(addToStart(students, 033));

// #3 
// Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument.
function elementChecker(students, str) {
    // If array is empty, return the message "error - passed in array is empty". 
    if (students.length === 0) {
        return 'error - passed in array is empty';
    } else {
        // Otherwise, return a boolean value depending on the result of the check.
        return students.includes(str);
    }
}

// Use elementChecker
console.log(elementChecker(students, "Jane"));

// Test for Error
console.log(elementChecker([], "Jane"));

// #4
// Create a function named checkAllStringsEnding that will accept a passed in array and a character.
// CHeck if: if array is empty, return "error - array must NOT be empty"
// if at least one array element is NOT a string, return "error - all array elements must be strings"
console.log("CHECK STUDENT DATA",students);
function checkAllStringsEnding(students, char) {
    // If array is empty, return the message "error - passed in array is empty". 
    if (students.length === 0) {
        return 'error - passed in array is empty';
    } 
    // if at least one array element is NOT a string, return "error - all array elements must be strings"
    else if (students.some(function(item) {
        return typeof item !== 'string';
    })) {
        return 'error - all array elements must be strings';
    } 
    // if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string" 
    else if (typeof char !== 'string') {
        return 'error - 2nd argument must be of data type string';
    }
    // if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
    else if (char.length > 1) {
        return 'error - 2nd argument must be a single character';
    }
    // if every element in the student array ends in the passed in character, return true. else return false.
    else if (students.every(function(item) {
        return item.endsWith(char);
    })) {
        return true;
    } else {
        return false;
    }

}

// Use checkAllStringsEnding
console.log(checkAllStringsEnding(students, "e"));
console.log(checkAllStringsEnding([], "e"));
console.log(checkAllStringsEnding(["Jane", 02], "e"));
console.log(checkAllStringsEnding(students, "el"));
console.log(checkAllStringsEnding(students, 4));

// #5
// Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. 
function stringLengthSorter(students) { 
    // if at least one element is not a string, return "error - all array elements must be strings"
    if (students.some(function(item) {
        return typeof item !== 'string';
    })) {
        return 'error - all array elements must be strings';
    } else {
        // Otherwise, return the sorted array.
        return students.sort(function(a, b) {
            return a.length - b.length;
        });
    }
}

// Use stringLengthSorter
console.log(stringLengthSorter(students));
console.log(stringLengthSorter([037, "John", 039, "Jane"])); 

// #6
// Create a function named startsWithCounter that will take in an array of strings and a single character. 
function startsWithCounter(students, char) {
    // If array is empty, return the message "error - passed in array is empty". 
    if (students.length === 0) {
        return 'error - passed in array is empty';
    } 
    // if at least one array element is NOT a string, return "error - all array elements must be strings"
    else if (students.some(function(item) {
        return typeof item !== 'string';
    })) {
        return 'error - all array elements must be strings';
    } 
    // if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string" 
    else if (typeof char !== 'string') {
        return 'error - 2nd argument must be of data type string';
    }
    // if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
    else if (char.length > 1) {
        return 'error - 2nd argument must be a single character';
    }
    // return the number of elements in the array that start with the character argument, must be case-insensitive
    else {
        return students.filter(function(item) {
            return item.toLowerCase().startsWith(char.toLowerCase());
        }).length;
    }   
}

// Use startsWithCounter
console.log(startsWithCounter(students, "J"));

// #7
// Create a function named likeFinder that will take in an array of strings and a string to be searched for. 
function likeFinder(students, str) {
    // If array is empty, return the message "error - passed in array is empty". 
    if (students.length === 0) {
        return 'error - passed in array is empty';
    } 
    // if at least one array element is NOT a string, return "error - all array elements must be strings"
    else if (students.some(function(item) {
        return typeof item !== 'string';
    })) {
        return 'error - all array elements must be strings';
    } 
    // if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string" 
    else if (typeof str !== 'string') {
        return 'error - 2nd argument must be of data type string';
    }
    // return a new array containing all elements of the array argument that have the string argument in it, must be case-insensitive
    else {
        return students.filter(function(item) {
            return item.toLowerCase().includes(str.toLowerCase());
        });
    }   
}

// Use likeFinder
console.log(likeFinder(students, "jo"));

//#8
// Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
function randomPicker(students) {
    return students[Math.floor(Math.random() * students.length)];
}

// Use randomPicker
console.log(randomPicker(students));
console.log(randomPicker(students));
console.log(randomPicker(students));
console.log(randomPicker(students));
console.log(randomPicker(students));